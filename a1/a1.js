let http = require("http");

http.createServer(function(request, response){

	if (request.url == "/" && request.method == "GET"){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to Booking System')
	} else if (request.url == "/profile" && request.method == "GET"){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to your profile!')
	} else if (request.url == "/courses" && request.method == "GET"){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Here are our courses available')

	} else if (request.url == "/addcourse" && request.method == "GET"){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Add a course to our resources")

	}



	if (request.url == "/updatecourse" && request.method == "POST") {

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Update a course in our resources")

	}  



	if (request.url == "/archivecourse" && request.method == "DELETE"){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Archive courses to our resources")

	}

}).listen(4000)

console.log("Activity Server running at localhost:4000");